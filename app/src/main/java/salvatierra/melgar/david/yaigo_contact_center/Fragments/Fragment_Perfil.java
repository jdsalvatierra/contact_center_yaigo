package salvatierra.melgar.david.yaigo_contact_center.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.navigation.NavigationView;

import salvatierra.melgar.david.yaigo_contact_center.R;
import salvatierra.melgar.david.yaigo_contact_center.Utilitis.Preferences;

public class Fragment_Perfil extends Fragment {

    TextView FuncionarioId, Nombre, Apellido, NombreUsuario, CedulaIdentidad;
    SharedPreferences preferences;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_perfil,container,false);
        preferences = getContext().getSharedPreferences("DatosUsuario", Context.MODE_PRIVATE);
        FuncionarioId = view.findViewById(R.id.FuncionarioId);
        Nombre = view.findViewById(R.id.Nombre);
        Apellido = view.findViewById(R.id.Apellido);
        NombreUsuario = view.findViewById(R.id.NombreUsuario);
        CedulaIdentidad = view.findViewById(R.id.CedulaIdentidad);
        //CUANDO ES ES INT Y ES STRING
        FuncionarioId.setText(Preferences.getfuncionarioId(preferences)+"");
        Nombre.setText(Preferences.getnombre(preferences)+"");
        Apellido.setText(Preferences.getapellido(preferences)+"");
        NombreUsuario.setText(Preferences.getnombreUsuario(preferences)+"");
        CedulaIdentidad.setText(Preferences.getcedulaIdentidad(preferences)+"");
        return view;


    }

}
