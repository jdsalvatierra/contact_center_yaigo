package salvatierra.melgar.david.yaigo_contact_center.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

import salvatierra.melgar.david.yaigo_contact_center.R;
import salvatierra.melgar.david.yaigo_contact_center.Retrofit.BaseUrl;

public class FullImageActivity extends AppCompatActivity {

    ImageView myImage, back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image);
        myImage = findViewById(R.id.image);
        back = findViewById(R.id.back);

        if(getIntent().getStringExtra("image")!=null){
            if(getIntent().getStringExtra("image").startsWith("http")){
                Glide.with(this)
                        .load(BaseUrl.URL_ARCHIVOS+getIntent().getStringExtra("image"))
                        .placeholder(R.color.codeGray)
                        .into(myImage);
            }else{
                Glide.with(this)
                        .load(getIntent().getStringExtra("image"))
                        .placeholder(R.color.codeGray)
                        .into(myImage);
            }

        }


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0,0);
            }
        });
    }
}
