package salvatierra.melgar.david.yaigo_contact_center.NotificacionesPush;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Vibrator;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.Random;

import salvatierra.melgar.david.yaigo_contact_center.Utilitis.PasoParametro;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.d("NEW_TOKEN",s);
        System.out.println(s);
    }
        public MyFirebaseMessagingService() {
            super();
        }

        @Override
        public void onMessageReceived(RemoteMessage remoteMessage){
            Log.d("FIREBASE-MSG", "onMessageReceived: " + remoteMessage.getData().toString());
            PasoParametro.Notificacion=true;
            Map data = remoteMessage.getData();

            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            Intent notificationIntent = new Intent(this, NotificacionReceiver.class);
            String urlimg = data.get("ulrimg").toString();
            int idNotificacion = new Random().nextInt(1000);
            byte[] byteArray = null;
            notificationIntent.putExtra("title", data.get("title").toString());
            notificationIntent.putExtra("tipo", data.get("tipo").toString());
            notificationIntent.putExtra("alert", data.get("alert").toString());
            notificationIntent.putExtra("ulrimg", urlimg);
            notificationIntent.putExtra("idContenido", data.get("idContenido").toString());
            notificationIntent.putExtra("idNotificacion", idNotificacion);
            if(urlimg.length() > 0) {
                Bitmap image = null;
                try {
                    URL url = new URL(urlimg);
                    image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    ByteArrayOutputStream bStream = new ByteArrayOutputStream();
                    image.compress(Bitmap.CompressFormat.PNG, 100, bStream);
                    byteArray = bStream.toByteArray();
                    notificationIntent.putExtra("image", byteArray);
                } catch (IOException e) {
                    System.out.println(e);
                }
            }
            Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
            vibrator.vibrate(250);
            //mostrarNotificacion(data, idNotificacion, byteArray);

            //Alarma de nueva notificacion
            PendingIntent broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            System.out.println("TEST - PROGRAMANDO ALARMA");
                AlarmManager.AlarmClockInfo ac= new AlarmManager.AlarmClockInfo(System.currentTimeMillis(),
                        broadcast);
                alarmManager.setAlarmClock(ac, broadcast);
            } else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), broadcast);
            } else {
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 30000, broadcast);
            }

        }




    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }

    @Override
    public void onMessageSent(String s) {
        super.onMessageSent(s);
    }

    @Override
    public void onSendError(String s, Exception e) {
        super.onSendError(s, e);
    }

}

