package salvatierra.melgar.david.yaigo_contact_center.Pojo;

public class RegistrarDispositivoPojo {
    public  int FuncionarioId;
    public String PushID;
    public String Tipo;

    public  RegistrarDispositivoPojo (int funcionarioId,String PushId,String tipo){
        FuncionarioId = funcionarioId;
        PushID = PushId;
        Tipo = tipo;
    }

}
