package salvatierra.melgar.david.yaigo_contact_center.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import salvatierra.melgar.david.yaigo_contact_center.Adapter.ListChatAdapter;

import salvatierra.melgar.david.yaigo_contact_center.Json.ListarEventoColaboradorJson;
import salvatierra.melgar.david.yaigo_contact_center.Pojo.ListarEventosColaboradorPojo;
import salvatierra.melgar.david.yaigo_contact_center.R;
import salvatierra.melgar.david.yaigo_contact_center.Retrofit.Cliente;
import salvatierra.melgar.david.yaigo_contact_center.Retrofit.ContactcenterService;
import salvatierra.melgar.david.yaigo_contact_center.Retrofit.Respuesta;
import salvatierra.melgar.david.yaigo_contact_center.Utilitis.Preferences;

public class Fragment_Contact_Center<iChatFragment> extends Fragment implements SearchView.OnQueryTextListener {

    RecyclerView recyclerView;
    SearchView txtBuscar;
    SharedPreferences preferences;
    ListChatAdapter ListChatAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_center,container,false);
        recyclerView = view.findViewById(R.id.recyclerListarMensajes);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        txtBuscar = view.findViewById(R.id.txtBuscar);
        preferences = getContext().getSharedPreferences("DatosUsuario", Context.MODE_PRIVATE);
        cargarLista();
        return view;
    }
    public void  cargarLista(){
        ListarEventosColaboradorPojo listareventocolaboradorPojo = new ListarEventosColaboradorPojo(Preferences.getfuncionarioId(preferences));
        ContactcenterService service =
                Cliente.getClient();
        Call<Respuesta<List<ListarEventoColaboradorJson>>> call = service.ListarEventoColaborador(listareventocolaboradorPojo);

        call.enqueue(new Callback<Respuesta<List<ListarEventoColaboradorJson>>>() {
            @Override
            public void onResponse(Call<Respuesta<List<ListarEventoColaboradorJson>>> call, Response<Respuesta<List<ListarEventoColaboradorJson>>> response) {
                if (response.isSuccessful()){
                    Respuesta<List<ListarEventoColaboradorJson>> respuesta = response.body();

                    if (respuesta.respuestaExitosa()){
                        ListChatAdapter = new ListChatAdapter(getContext(),respuesta.getData());
                        recyclerView.setAdapter(ListChatAdapter);
                    }else {
                        Toast.makeText(getActivity(), "Error"+response.message(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Respuesta<List<ListarEventoColaboradorJson>>> call, Throwable t) {
                System.out.print("error_ONFAIL "+ t.getMessage() );
            }
        });

        txtBuscar.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        ListChatAdapter.filtarlista(s);
        return false;
    }



}
