

package salvatierra.melgar.david.yaigo_contact_center.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import salvatierra.melgar.david.yaigo_contact_center.Adapter.ChatAdapter;
import salvatierra.melgar.david.yaigo_contact_center.Adapter.ListObtenerColaboradorAdapter;
import salvatierra.melgar.david.yaigo_contact_center.Json.ListarMensajePorHiloJson;
import salvatierra.melgar.david.yaigo_contact_center.Json.ObtenerColaboradorJson;
import salvatierra.melgar.david.yaigo_contact_center.Pojo.ListarMensajePorHiloPojo;
import salvatierra.melgar.david.yaigo_contact_center.Pojo.MarcarComoAtendidoPojo;
import salvatierra.melgar.david.yaigo_contact_center.Pojo.MarcarComoNoResueltoPojo;
import salvatierra.melgar.david.yaigo_contact_center.Pojo.ObtenerColaboradorPojo;
import salvatierra.melgar.david.yaigo_contact_center.Pojo.ReasignarColaboradorPojo;
import salvatierra.melgar.david.yaigo_contact_center.R;
import salvatierra.melgar.david.yaigo_contact_center.Retrofit.Cliente;
import salvatierra.melgar.david.yaigo_contact_center.Retrofit.ContactcenterService;
import salvatierra.melgar.david.yaigo_contact_center.Retrofit.Respuesta;
import salvatierra.melgar.david.yaigo_contact_center.Utilitis.PasoParametro;
import salvatierra.melgar.david.yaigo_contact_center.Utilitis.Preferences;
import salvatierra.melgar.david.yaigo_contact_center.Activity.activity_images;


public class Fragment_Chat extends Fragment {
    RecyclerView recyclerView;
    Context context;
    ChatAdapter ChatAdapter;
    SharedPreferences preferences;
    ImageButton btnAtras,btnEnviar,btnOpciones, btnsubirImagen ;
    ImageView ImagenChat;
    Retrofit retrofit;
    EditText et_chat_texto;
    TextView txtNombrecolaborador;
    CircleImageView cv_foto_cliente;
    ListObtenerColaboradorAdapter ListObtenerColaboradorAdapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        recyclerView = view.findViewById(R.id.recyclerChatMensajes);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        btnAtras = (ImageButton) view.findViewById(R.id.btnAtras);
        btnEnviar = (ImageButton) view.findViewById(R.id.btnEnviar);
        btnsubirImagen = (ImageButton) view.findViewById(R.id.btnsubirImagen);
        btnOpciones = (ImageButton) view.findViewById(R.id.btnOpciones);
        et_chat_texto = view.findViewById(R.id.et_chat_texto);
        ImagenChat = view.findViewById(R.id.ImagenChat);
        txtNombrecolaborador= view.findViewById(R.id.txtNombrecolaborador);
        preferences = getContext().getSharedPreferences("DatosUsuario", Context.MODE_PRIVATE);
        txtNombrecolaborador.setText(PasoParametro.NOMBRE_CLIENTE);


        /*
          Glide.with(context)
                .load(BaseUrl.baseUrl+listarEventoColaboradorJson.geturlfotoPerfil())
                .centerCrop().dontAnimate()
                .placeholder(R.drawable.colaborador)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .signature(new ObjectKey(String.valueOf(System.currentTimeMillis())))
                .into(holder.Imgcolaborador);
                */

        cargarChat();
        //------------------------- para volver atras a la lista de chat
        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requireActivity().onBackPressed();
            }
        });

        //BOTON PARA ENVIAR LO QUE UNO ESCRIBE EN EL CHAT
        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!et_chat_texto.getText().toString().equals(""))
                    EnviarMensajes();

            }
        });

        btnsubirImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getContext(), activity_images.class ).putExtra("texto", et_chat_texto.getText().toString());
                getContext().startActivity(intent);

            }
        });


        //DESDE AQUI ES COMO OTRO FAGMENT DONDE MUESTRAS LAS OPCIONES DE CHAT
        //MARCAR LEIDO NO LEIDO ETC..
        btnOpciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Dialog dialog= new Dialog(getContext());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.fragment_opciones_del_chat);
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Button btnMarcarNoResuelto = dialog.findViewById(R.id.btnMarcarNoResuelto);
                Button btnMarcarResuelto = dialog.findViewById(R.id.btnMarcarResuelto);
                Spinner SpinnerlistaColaboradores = dialog.findViewById(R.id.SpinnerlistaColaboradores);
                Button btnReasignar = dialog.findViewById(R.id.btnReasignar);
                Button btnCancelar = dialog.findViewById(R.id.btnCancelar);
                obtenerColaborador(SpinnerlistaColaboradores);
                dialog.show();


                btnMarcarNoResuelto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            marcarnoResuelto(dialog);
                        } catch (Exception e) {
                            Toast.makeText(getActivity(), "HUBO UN ERROR INESPERADO", Toast.LENGTH_LONG).show();


                        }
                    }
                });

                btnMarcarResuelto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            marcarResuelto(dialog);
                        } catch (Exception e) {
                            Toast.makeText(getActivity(), "HUBO UN ERROR INESPERADO", Toast.LENGTH_LONG).show();


                        }
                    }
                });

                btnReasignar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        boolean encontrado = false;
                        int funcionarioId = 0;
                        for (int i = 0; i<obtenerColaboradorJsons.size();i++) {
                        if (!encontrado){
                            String funcionario= obtenerColaboradorJsons.get(i).getnombre()+" | "+ obtenerColaboradorJsons.get(i).getarea()+" | "+ obtenerColaboradorJsons.get(i).getciudad();
                            if (funcionario.equals( SpinnerlistaColaboradores.getSelectedItem().toString())){
                                encontrado = true;
                                funcionarioId = obtenerColaboradorJsons.get(i).getfuncionarioId();

                                try {
                                    reasignar(funcionarioId,dialog);
                                } catch (Exception e) {
                                    Toast.makeText(getActivity(), "HUBO UN ERROR INESPERADO", Toast.LENGTH_LONG).show();

                                }
                            }
                         }
                        }
                    }
                });

                btnCancelar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss ();
                    }
                });
            }
        });



        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        cargarChat();
    }

    public void marcarnoResuelto(Dialog dialog) throws Exception {
        MarcarComoNoResueltoPojo marcarComoNoResueltoPojo = new MarcarComoNoResueltoPojo(Integer.parseInt(PasoParametro.EventoHiloId));
        ContactcenterService service =
                Cliente.getClient();
        Call<Respuesta<String>> call = service.marcarComoNoResuelto(marcarComoNoResueltoPojo);

        call.enqueue(new Callback<Respuesta<String>>() {
            @Override
            public void onResponse(Call<Respuesta<String>> call, Response<Respuesta<String>> response) {
                if (response.isSuccessful()){
                    Respuesta<String> respuesta = response.body();

                    if (respuesta.respuestaExitosa()){
                        Toast.makeText(getActivity(), respuesta.getData()+"", Toast.LENGTH_LONG).show();
                        dialog.dismiss ();
                    }
                }
            }

            @Override
            public void onFailure(Call<Respuesta<String>> call, Throwable t) {
                System.out.print("error_ONFAIL "+ t.getMessage() );
            }
        });
    }

    public void marcarResuelto(Dialog dialog) throws Exception {
        MarcarComoAtendidoPojo marcarComoAtendidoPojo = new MarcarComoAtendidoPojo(Integer.parseInt(PasoParametro.EventoHiloId));
        ContactcenterService service =
                Cliente.getClient();
        Call<Respuesta<String>> call = service.marcarComoAtentido(marcarComoAtendidoPojo);

        call.enqueue(new Callback<Respuesta<String>>() {
            @Override
            public void onResponse(Call<Respuesta<String>> call, Response<Respuesta<String>> response) {
                if (response.isSuccessful()){
                    Respuesta<String> respuesta = response.body();

                    if (respuesta.respuestaExitosa()){
                        Toast.makeText(getActivity(), ""+respuesta.getData(), Toast.LENGTH_LONG).show();
                        dialog.dismiss ();
                    }
                }
            }

            @Override
            public void onFailure(Call<Respuesta<String>> call, Throwable t) {
                System.out.print("error_ONFAIL "+ t.getMessage() );
            }
        });
    }

    List<ObtenerColaboradorJson> obtenerColaboradorJsons = new ArrayList<>();
    public void obtenerColaborador(Spinner spinner){
        ObtenerColaboradorPojo obtenerColaboradorPojo = new ObtenerColaboradorPojo(PasoParametro.Tipo);
        ContactcenterService service =
                Cliente.getClient();
        Call<Respuesta<List<ObtenerColaboradorJson>>> call = service.ObtenerColaborador(obtenerColaboradorPojo);
        call.enqueue(new Callback<Respuesta<List<ObtenerColaboradorJson>>>() {
            @Override
            public void onResponse(Call<Respuesta<List<ObtenerColaboradorJson>>> call, Response<Respuesta<List<ObtenerColaboradorJson>>> response) {
                if (response.isSuccessful()){
                    Respuesta<List<ObtenerColaboradorJson>> respuesta = response.body();
                    if (respuesta.getData().size()>0){
                        obtenerColaboradorJsons.addAll(respuesta.getData());
                        List<String> stringList = new ArrayList<>();
                        stringList.add("Selecciona un colabadorador");
                        for (int i = 0; i<respuesta.getData().size();i++){
                            stringList.add(respuesta.getData().get(i).getnombre()+" | "+ respuesta.getData().get(i).getarea()+" | "+ respuesta.getData().get(i).getciudad());

                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item,stringList);
                        spinner.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<Respuesta<List<ObtenerColaboradorJson>>> call, Throwable t) {
                System.out.print("error_ONFAIL "+ t.getMessage() );
            }
        });
    }

    public void reasignar(int funcionarioId,Dialog dialog) throws Exception {
        ReasignarColaboradorPojo reasignarColaboradorPojo = new ReasignarColaboradorPojo(Integer.parseInt(PasoParametro.EventoHiloId),funcionarioId);
        ContactcenterService service =
                Cliente.getClient();
        Call<Respuesta<String>> call = service.reasignarColaborador(reasignarColaboradorPojo);
        call.enqueue(new Callback<Respuesta<String>>() {
            @Override
            public void onResponse(Call<Respuesta<String>> call, Response<Respuesta<String>> response) {
                if (response.isSuccessful()){
                    Respuesta<String> respuesta = response.body();
                    if (respuesta.respuestaExitosa()){
                        Toast.makeText(getActivity(), ""+respuesta.getData(), Toast.LENGTH_LONG).show();
                        dialog.dismiss ();
                    }
                }
            }

            @Override
            public void onFailure(Call<Respuesta<String>> call, Throwable t) {
                System.out.print("error_ONFAIL "+ t.getMessage() );
            }
        });
    }

    //CARGA EL SERVICIO DE LOS CHAT QUE ENVIAN Y ENVIO
    public void cargarChat(){
        try{
            ListarMensajePorHiloPojo listarMensajePorHiloPojo = new ListarMensajePorHiloPojo(Integer.parseInt(PasoParametro.EventoHiloId));
            ContactcenterService service =
                    Cliente.getClient();
            Call<Respuesta<List<ListarMensajePorHiloJson>>> call = service.ListarMensajePorHilo(listarMensajePorHiloPojo);
            call.enqueue(new Callback<Respuesta<List<ListarMensajePorHiloJson>>>() {
                @Override
                public void onResponse(Call<Respuesta<List<ListarMensajePorHiloJson>>> call, Response<Respuesta<List<ListarMensajePorHiloJson>>> response) {
                    if (response.isSuccessful()){
                        Respuesta<List<ListarMensajePorHiloJson>> respuesta = response.body();

                        if (respuesta.respuestaExitosa()){
                            ChatAdapter = new ChatAdapter(getContext(),respuesta.getData());
                            recyclerView.setAdapter(ChatAdapter);
                        }else {
                            Toast.makeText(getActivity(), "Error"+response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<Respuesta<List<ListarMensajePorHiloJson>>> call, Throwable t) {
                    System.out.print("error_ONFAIL "+ t.getMessage() );
                }
            });
        }
        catch (Exception ex){
            Toast.makeText(getContext(),"Error inesperado al intentar obtener los mensajes del chat",Toast.LENGTH_SHORT).show();
        }

    }

    //CARGA EL SERVICIO PARA YO ENVIAR MENSAJES
    public void EnviarMensajes(){

        btnEnviar.setEnabled(false);
        ContactcenterService service = Cliente.getClient();
        RequestBody EventoHiloId = RequestBody.create(okhttp3.MultipartBody.FORM, PasoParametro.EventoHiloId);
        RequestBody Texto = RequestBody.create(okhttp3.MultipartBody.FORM,et_chat_texto.getText().toString());

        Call<Respuesta<String>>call = service.EnviarMensajePorEventoPorHiloJson(EventoHiloId,Texto,null);
        call.enqueue(new Callback<Respuesta<String>>() {
            @Override
            public void onResponse(Call<Respuesta<String>> call, Response<Respuesta<String>> response) {
                if (response.isSuccessful()){
                    Respuesta<String>respuesta= response.body();
                    if (respuesta.respuestaExitosa()){
                        btnEnviar.setEnabled(true);
                        et_chat_texto.setText("");
                        cargarChat();
                    }else{
                        Toast.makeText(getActivity(), "Error"+response.message(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Respuesta<String>> call, Throwable t) {
                Toast.makeText(getActivity(), "ERROR", Toast.LENGTH_LONG).show();
            }
        });


    }




}
