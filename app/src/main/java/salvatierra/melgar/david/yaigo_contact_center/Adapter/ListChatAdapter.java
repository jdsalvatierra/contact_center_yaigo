package salvatierra.melgar.david.yaigo_contact_center.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.ObjectKey;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import de.hdodenhof.circleimageview.CircleImageView;
import salvatierra.melgar.david.yaigo_contact_center.Activity.Activity_Menulateral;
import salvatierra.melgar.david.yaigo_contact_center.Fragments.Fragment_Chat;
import salvatierra.melgar.david.yaigo_contact_center.Json.ListarEventoColaboradorJson;
import salvatierra.melgar.david.yaigo_contact_center.R;
import salvatierra.melgar.david.yaigo_contact_center.Retrofit.BaseUrl;
import salvatierra.melgar.david.yaigo_contact_center.Utilitis.PasoParametro;

public class ListChatAdapter extends RecyclerView.Adapter<ListChatAdapter.ViewHolder> {
    Context context;
    List<ListarEventoColaboradorJson> eventoColaboradorJsons;
    List<ListarEventoColaboradorJson> eventoColaboradorJsonsBuscar;


    public ListChatAdapter(Context context, List<ListarEventoColaboradorJson> eventoColaboradorJsons) {
        this.context = context;
        this.eventoColaboradorJsons = eventoColaboradorJsons;
        eventoColaboradorJsonsBuscar = new ArrayList<>();
        eventoColaboradorJsonsBuscar.addAll(eventoColaboradorJsons);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_lista_mensajes, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ListarEventoColaboradorJson listarEventoColaboradorJson = eventoColaboradorJsons.get(position);
        holder.txtNombrecolaborador.setText(listarEventoColaboradorJson.getnombrecliente());
        holder.txtMensajecolaborador.setText(listarEventoColaboradorJson.getmensaje());
        holder.txtHoracolaborador.setText(listarEventoColaboradorJson.getfechahora().replace("T","  "));

        Glide.with(context)
                .load(BaseUrl.baseUrl+listarEventoColaboradorJson.geturlfotoPerfil())
                .centerCrop().dontAnimate()
                .placeholder(R.drawable.colaborador)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .signature(new ObjectKey(String.valueOf(System.currentTimeMillis())))
                .into(holder.Imgcolaborador);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Activity_Menulateral)context).goFragment(new Fragment_Chat(),"");
                PasoParametro.EventoHiloId = String.valueOf(listarEventoColaboradorJson.geteventohiloId());
                PasoParametro.NOMBRE_CLIENTE= listarEventoColaboradorJson.getnombrecliente();
            }
        });

    }

    public void filtarlista(String txtBuscar){
        int longitud = txtBuscar.length();
        if (txtBuscar.length() == 0) {
            eventoColaboradorJsons.clear();
            eventoColaboradorJsons.addAll(eventoColaboradorJsonsBuscar);
        }else{
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                List<ListarEventoColaboradorJson> coleccion = eventoColaboradorJsons.stream()
                        .filter(i -> i.getnombrecliente().toLowerCase().contains(txtBuscar.toLowerCase()))
                        .collect(Collectors.toList());
                eventoColaboradorJsons.clear();
                eventoColaboradorJsons.addAll(coleccion);
            } else {
                for (ListarEventoColaboradorJson c: eventoColaboradorJsonsBuscar) {
                    if (c.getnombrecliente().toLowerCase().contains(txtBuscar.toLowerCase())){
                        eventoColaboradorJsons.add(c);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return eventoColaboradorJsons.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtNombrecolaborador,txtMensajecolaborador, txtHoracolaborador ;
        CircleImageView Imgcolaborador;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNombrecolaborador = itemView.findViewById(R.id.txtNombrecolaborador);
            txtMensajecolaborador = itemView.findViewById(R.id.txtMensajecolaborador);
            Imgcolaborador = itemView.findViewById(R.id.Imgcolaborador);
            txtHoracolaborador = itemView.findViewById(R.id.txtHoracolaborador);
        }
    }
}
