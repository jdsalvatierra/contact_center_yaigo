package salvatierra.melgar.david.yaigo_contact_center.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import salvatierra.melgar.david.yaigo_contact_center.Json.LoginJson;
import salvatierra.melgar.david.yaigo_contact_center.Pojo.LoginPojo;
import salvatierra.melgar.david.yaigo_contact_center.R;
import salvatierra.melgar.david.yaigo_contact_center.Retrofit.Cliente;
import salvatierra.melgar.david.yaigo_contact_center.Retrofit.ContactcenterService;
import salvatierra.melgar.david.yaigo_contact_center.Retrofit.Respuesta;
import salvatierra.melgar.david.yaigo_contact_center.Utilitis.Preferences;


public class Activity_Login extends AppCompatActivity {
    EditText txtUsuariologin, txtPasswordlogin ;
    Button  btnIngresar;
    Preferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        preferences = new Preferences(Activity_Login.this);
        txtUsuariologin = findViewById(R.id.txtUsuariologin);
        txtPasswordlogin = findViewById(R.id.txtPasswordlogin);
        btnIngresar=(Button)findViewById(R.id.btnIngresar);



        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validarDatos()){
                login();
                }
            }
        });
    }


    public void login() {
        SweetAlertDialog sweetAlertDialog= new SweetAlertDialog(Activity_Login.this, SweetAlertDialog.PROGRESS_TYPE);
        sweetAlertDialog.setTitle("Cargando...");
        sweetAlertDialog.show();
        ContactcenterService service =
                Cliente.getClient();

        LoginPojo loginpojo = new LoginPojo(txtUsuariologin.getText().toString(), txtPasswordlogin.getText().toString());
        Call<Respuesta<LoginJson>> call = service.login(loginpojo);
        call.enqueue(new Callback<Respuesta<LoginJson>>() {
            @Override
            public void onResponse(Call<Respuesta<LoginJson>> call, Response<Respuesta<LoginJson>> response) {
                if (response.isSuccessful()) {
                    Respuesta<LoginJson> respuesta = response.body();

                    sweetAlertDialog.dismiss();
                    if (respuesta.respuestaExitosa()) {
                        GuardarDatos(respuesta.getData());
                        Intent btnIngresar = new Intent(Activity_Login.this, Activity_Menulateral.class);
                        startActivity(btnIngresar);
                        Activity_Login.this.finish();


                    } else {
                        SweetAlertDialog sweetAlertDialog= new SweetAlertDialog(Activity_Login.this, SweetAlertDialog.ERROR_TYPE);
                        sweetAlertDialog.setTitle("Aviso");
                        sweetAlertDialog.setTitleText(respuesta.getMensaje());
                        sweetAlertDialog.show();
                    }
                }else{
                    sweetAlertDialog.dismiss();
                    Toast.makeText(Activity_Login.this,"response_error"+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Respuesta<LoginJson>> call, Throwable t) {
                sweetAlertDialog.dismiss();
                Toast.makeText(getApplicationContext(),t.getMessage(),  Toast.LENGTH_LONG).show();
                System.out.print("error_ONFAIL "+ t.getMessage() );
            }

        });
    }
    public Boolean validarDatos(){
        if(txtUsuariologin.getText().toString().equals("")){
            txtUsuariologin.setError("INGRESAR DATOS");
            txtUsuariologin.requestFocus();
            return false;
        }
        if(txtPasswordlogin.getText().toString().equals("")){
            txtPasswordlogin.setError("INGRESAR DATOS");
            txtPasswordlogin.requestFocus();
            return false;
        }
        return true;
    }

    public void GuardarDatos(LoginJson json){
        SharedPreferences prefsLogin = getSharedPreferences("DatosUsuario", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefsLogin.edit();
        //inicio de sesion
        editor.putBoolean("iniciarSesion", true);
        //Inicio de Sesion datos usuario
        editor.putInt("FuncionarioId", json.getfuncionarioId());
        editor.putString("Nombre", json.getnombre());
        editor.putString("Apellido", json.getapellido());
        editor.putString("NombreUsuario",json.getnombreUsuario());
        editor.putString("CedulaIdentidad",json.getcedulaIdentidad());
        editor.putString("FotoPerfil",json.getfotoPerfil());
        editor.apply();
    }

}
