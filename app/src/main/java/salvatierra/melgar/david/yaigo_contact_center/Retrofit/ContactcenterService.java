package salvatierra.melgar.david.yaigo_contact_center.Retrofit;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import salvatierra.melgar.david.yaigo_contact_center.Json.ListarEventoColaboradorJson;
import salvatierra.melgar.david.yaigo_contact_center.Json.ListarMensajePorHiloJson;
import salvatierra.melgar.david.yaigo_contact_center.Json.LoginJson;
import salvatierra.melgar.david.yaigo_contact_center.Json.ObtenerColaboradorJson;
import salvatierra.melgar.david.yaigo_contact_center.Pojo.ListarEventosColaboradorPojo;
import salvatierra.melgar.david.yaigo_contact_center.Pojo.ListarMensajePorHiloPojo;
import salvatierra.melgar.david.yaigo_contact_center.Pojo.LoginPojo;
import salvatierra.melgar.david.yaigo_contact_center.Pojo.MarcarComoAtendidoPojo;
import salvatierra.melgar.david.yaigo_contact_center.Pojo.MarcarComoNoResueltoPojo;
import salvatierra.melgar.david.yaigo_contact_center.Pojo.ObtenerColaboradorPojo;
import salvatierra.melgar.david.yaigo_contact_center.Pojo.ReasignarColaboradorPojo;
import salvatierra.melgar.david.yaigo_contact_center.Pojo.RegistrarDispositivoPojo;

public interface ContactcenterService {
    @POST("colaborador/login")
    Call<Respuesta<LoginJson>> login(@Body LoginPojo loginPojo);
    @POST("colaborador/listarPorColaborador")
    Call<Respuesta<List<ListarEventoColaboradorJson>>> ListarEventoColaborador(@Body ListarEventosColaboradorPojo listareventoscolaboradorPojo);
    @POST("colaborador/listarMensajesEventoHilo")
    Call<Respuesta<List<ListarMensajePorHiloJson>>> ListarMensajePorHilo(@Body ListarMensajePorHiloPojo listarmensajeporhiloPojo);

    @Multipart
    @POST("colaborador/enviarMensajePorEventoPorHilo")
    Call<Respuesta<String>> EnviarMensajePorEventoPorHiloJson(
            @Part("EventoHiloId")RequestBody EventoHiloId,
            @Part("Texto") RequestBody Texto,
            @Part List<MultipartBody.Part> Archivo
    );

    @POST("colaborador/obtenerListaColaboradores")
    Call<Respuesta<List<ObtenerColaboradorJson>>> ObtenerColaborador(@Body ObtenerColaboradorPojo obtenerColaboradorPojo);
    @POST("colaborador/reasignarColaborador")
    Call<Respuesta<String>> reasignarColaborador(@Body ReasignarColaboradorPojo reasignarColaboradorPojo);
    @POST("colaborador/marcarComoNoResuelto")
    Call<Respuesta<String>> marcarComoNoResuelto(@Body MarcarComoNoResueltoPojo marcarComoNoResueltoPojo);
    @POST("colaborador/marcarComoAtendido")
    Call<Respuesta<String>> marcarComoAtentido(@Body MarcarComoAtendidoPojo marcarComoAtendidoPojo);
    @POST("colaborador/RegistrarDispositivoColaborador")
    Call<Respuesta<String>> registrarDispositivo (@Body RegistrarDispositivoPojo registrarDispositivoPojo);


}
