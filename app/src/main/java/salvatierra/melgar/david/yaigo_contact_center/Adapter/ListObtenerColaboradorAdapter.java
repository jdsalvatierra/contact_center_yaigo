package salvatierra.melgar.david.yaigo_contact_center.Adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import salvatierra.melgar.david.yaigo_contact_center.Json.ObtenerColaboradorJson;
import salvatierra.melgar.david.yaigo_contact_center.R;


public class ListObtenerColaboradorAdapter extends RecyclerView.Adapter<ListObtenerColaboradorAdapter.ViewHolder> {
    Context context;
    List<ObtenerColaboradorJson> obtenerColaboradorJsons;

    public ListObtenerColaboradorAdapter(Context context, List<ObtenerColaboradorJson> obtenerColaboradorJsons){
        this.context = context;
        this.obtenerColaboradorJsons = obtenerColaboradorJsons;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_opciones_del_chat, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ObtenerColaboradorJson obtenerColaboradorJson = obtenerColaboradorJsons.get(position);
    }

    @Override
    public int getItemCount() {
        return obtenerColaboradorJsons.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
