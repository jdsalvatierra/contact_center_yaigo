package salvatierra.melgar.david.yaigo_contact_center.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.messaging.FirebaseMessaging;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import salvatierra.melgar.david.yaigo_contact_center.Fragments.Fragment_Contact_Center;
import salvatierra.melgar.david.yaigo_contact_center.Fragments.Fragment_Perfil;
import salvatierra.melgar.david.yaigo_contact_center.Pojo.RegistrarDispositivoPojo;
import salvatierra.melgar.david.yaigo_contact_center.R;
import salvatierra.melgar.david.yaigo_contact_center.Retrofit.Cliente;
import salvatierra.melgar.david.yaigo_contact_center.Retrofit.ContactcenterService;
import salvatierra.melgar.david.yaigo_contact_center.Retrofit.Respuesta;
import salvatierra.melgar.david.yaigo_contact_center.Utilitis.PasoParametro;
import salvatierra.melgar.david.yaigo_contact_center.Utilitis.Preferences;

public class Activity_Menulateral extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    //MENU
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationview;
    SharedPreferences preferences;
    //VARIABLE PARA CARGAR EL FAGMENT PRINCIPAL
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawermenulateral);
        preferences = getSharedPreferences("DatosUsuario", Context.MODE_PRIVATE);
        //--------------------------------------------------------------------------------------

        //para mostar bien las imagenes y que no salgan cuadros negros
        NavigationView navigationView = findViewById(R.id.navigationView);
        navigationView.setItemIconTintList(null);
        TextView NombreFuncionario= navigationView.getHeaderView(0).findViewById(R.id.NombreFuncionario);
        NombreFuncionario.setText(Preferences.getnombre(preferences));
        //----------------------------------------------------------------------------------
        drawerLayout = findViewById(R.id.drawermenu);
        navigationView = findViewById(R.id.navigationView);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        actionBarDrawerToggle.syncState();

        //AHORA AQUI VAMOS A CARGAR EL FRAGMENT PRINCIPAL
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.Contenedormenu, new Fragment_Contact_Center());
        fragmentTransaction.commit();
        //Establecer evento onClick al navigationView
        navigationView.setNavigationItemSelectedListener(this);

        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            System.out.print("TOKEN-FIREBASE--"+ task.getException());

                            return;
                        }

                        // Get new FCM registration token
                        String token = task.getResult();
                        RegistrarDispositivo(token);
                        System.out.print("TOKEN-FIREBASE--"+ token);
                        //Toast.makeText(Activity_Menulateral.this, token, Toast.LENGTH_SHORT).show();
                    }
                });


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        drawerLayout.closeDrawer(GravityCompat.START);

        if (menuItem.getItemId() == R.id.menuContact_center){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.Contenedormenu, new Fragment_Contact_Center());
            fragmentTransaction.commit();
        }

        if (menuItem.getItemId() == R.id.menuPerfil){
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.Contenedormenu, new Fragment_Perfil());
            fragmentTransaction.commit();
        }

        if (menuItem.getItemId() == R.id.menuCerrar_sesion){
            finish();

        }


        return false;
    }
    public void goFragment(Fragment fragment, String name) {

        getSupportFragmentManager().beginTransaction().replace(R.id.Contenedormenu, fragment, "fragmento").addToBackStack(name)
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit();
        int count = getSupportFragmentManager().getBackStackEntryCount();
        System.out.println("NUMERO DE LA PILA ACTUAL " + count);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }




    public void RegistrarDispositivo(String token){
        RegistrarDispositivoPojo registrarDispositivoPojo = new RegistrarDispositivoPojo
                (Preferences.getfuncionarioId(preferences),token,"ANDROID");
        ContactcenterService service =
                Cliente.getClient();
        Call<Respuesta<String>> call = service.registrarDispositivo(registrarDispositivoPojo);

        call.enqueue(new Callback<Respuesta<String>>() {
            @Override
            public void onResponse(Call<Respuesta<String>> call, Response<Respuesta<String>> response) {
                if (response.isSuccessful()){
                    Respuesta<String> respuesta = response.body();
                    if (!respuesta.respuestaExitosa()){
                        Toast.makeText(Activity_Menulateral.this, respuesta.getMensaje(), Toast.LENGTH_LONG).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<Respuesta<String>> call, Throwable t) {
                System.out.print("error_ONFAIL "+ t.getMessage() );
            }
        });
    }
}
