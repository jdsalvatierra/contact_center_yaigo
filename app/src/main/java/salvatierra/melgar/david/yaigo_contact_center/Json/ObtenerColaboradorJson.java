package salvatierra.melgar.david.yaigo_contact_center.Json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ObtenerColaboradorJson {
    @SerializedName("FuncionarioId")
    @Expose
    public Integer FuncionarioId;
    @SerializedName("Nombre")
    @Expose
    public String Nombre;
    @SerializedName("Apellido")
    @Expose
    public  String Apellido;
    @SerializedName("NombreUsuario")
    @Expose
    public  String NombreUsuario;
    @SerializedName("CedulaIdentidad")
    @Expose
    public String CedulaIdentidad;
    @SerializedName("FotoPerfil")
    @Expose
    public  String FotoPerfil;
    @SerializedName("ciudad")
    @Expose
    public  String ciudad;
    @SerializedName("area")
    @Expose
    public  String area;

    public Integer getfuncionarioId() {
        return FuncionarioId;
    }
    public String getnombre() {
        return Nombre;
    }
    public String getapellido() {
        return Apellido;
    }
    public String getnombreUsuario() {
        return NombreUsuario;
    }
    public String getcedulaIdentidad() {
        return CedulaIdentidad;
    }
    public String getfotoPerfil() {
        return FotoPerfil;
    }
    public String getciudad() {
        return ciudad;
    }
    public String getarea() {
        return area;
    }


}
