package salvatierra.melgar.david.yaigo_contact_center.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.ObjectKey;

import java.util.List;

import salvatierra.melgar.david.yaigo_contact_center.Activity.FullImageActivity;
import salvatierra.melgar.david.yaigo_contact_center.Json.ListarMensajePorHiloJson;
import salvatierra.melgar.david.yaigo_contact_center.R;
import salvatierra.melgar.david.yaigo_contact_center.Retrofit.BaseUrl;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    Context context;
    List<ListarMensajePorHiloJson> ListarMensajePorHiloJson;


    public ChatAdapter(Context context, List<ListarMensajePorHiloJson> ListarMensajePorHiloJson ){
        this.context = context;
        this.ListarMensajePorHiloJson = ListarMensajePorHiloJson;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.mesajes_chat, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ListarMensajePorHiloJson listarMensajePorHiloJson = ListarMensajePorHiloJson.get(position);
        holder.tv_card_message_chat.setText(listarMensajePorHiloJson.getMensaje());
        holder.txtHora.setText(listarMensajePorHiloJson.getFechaCreacion().replace("T","  "));
        if(!listarMensajePorHiloJson.getArchivo().equals("")){
            if(listarMensajePorHiloJson.getArchivo().startsWith("http")){
                holder.tv_card_message_chat.setVisibility(View.GONE);
                holder.ImagenChat.setVisibility(View.VISIBLE);
                Glide.with(context)
                        .load(listarMensajePorHiloJson.getArchivo())
                        .centerCrop().dontAnimate()
                        //.placeholder(R.drawable.yaigo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .signature(new ObjectKey(String.valueOf(System.currentTimeMillis())))
                        .into(holder.ImagenChat);
            }else{
                holder.tv_card_message_chat.setVisibility(View.GONE);
                holder.ImagenChat.setVisibility(View.VISIBLE);
                Glide.with(context)
                        .load(listarMensajePorHiloJson.getArchivo())
                        .centerCrop().dontAnimate()
                        //.placeholder(R.drawable.yaigo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .signature(new ObjectKey(String.valueOf(System.currentTimeMillis())))
                        .into(holder.ImagenChat);
            }

        }else{
            holder.tv_card_message_chat.setVisibility(View.VISIBLE);
            holder.ImagenChat.setVisibility(View.GONE);
        }

        holder.ImagenChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, FullImageActivity.class).putExtra("image",listarMensajePorHiloJson.getArchivo()));
            }
        });


        if (listarMensajePorHiloJson.Funcionario.getFuncionarioId()!=0){
            //MENSAJE FUNCIONARIO
            holder.tv_card_message_chat.setBackground(context.getDrawable(R.drawable.textview_mensaje));
            holder.tv_card_message_chat.setTextColor(context.getResources().getColor(R.color.white));
            //move letf
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.START;
            holder.tv_card_message_chat.setLayoutParams(params);
            holder.txtHora.setLayoutParams(params);
            holder.ImagenChat.setLayoutParams(params);
        }else {
            //MENSAJE CLIENTE
            holder.tv_card_message_chat.setBackground(context.getResources().getDrawable(R.drawable.textview_mensaje_pregunta));
            holder.tv_card_message_chat.setTextColor(context.getResources().getColor(R.color.white));
            //move rith
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.END;
            holder.tv_card_message_chat.setLayoutParams(params);
            holder.txtHora.setLayoutParams(params);
            holder.ImagenChat.setLayoutParams(params);
        }

    }

    @Override
    public int getItemCount() {
        return ListarMensajePorHiloJson.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_card_message_chat, txtHora;
        ImageView ImagenChat;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_card_message_chat = itemView.findViewById(R.id.tv_card_message_chat);
            txtHora = itemView.findViewById(R.id.txtHora);
            ImagenChat = itemView.findViewById(R.id.ImagenChat);
        }
    }
}
