package salvatierra.melgar.david.yaigo_contact_center.Json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListarEventoColaboradorJson {
    @SerializedName("EventoHiloId")
    @Expose
    public Integer EventoHiloId;
    @SerializedName("Mensaje")
    @Expose
    public String Mensaje;
    @SerializedName("NombreCliente")
    @Expose
    public String NombreCliente;
    @SerializedName("UrlFotoPerfil")
    @Expose
    public String UrlFotoPerfil;
    @SerializedName("FechaHora")
    @Expose
    public String FechaHora;
    @SerializedName("FuncionarioId")
    @Expose
    public Integer FuncionarioId;
    @SerializedName("ClienteId")
    @Expose
    public Integer ClienteId;

    public Integer geteventohiloId() {
        return EventoHiloId;
    }
    public String getmensaje() {
        return Mensaje;
    }
    public String getnombrecliente() {
        return NombreCliente;
    }
    public String geturlfotoPerfil() {
        return UrlFotoPerfil;
    }
    public String getfechahora() {
        return FechaHora;
    }
    public Integer getfuncionarioId() {
        return FuncionarioId;
    }
    public Integer getclienteId() {
        return ClienteId;
    }
}
