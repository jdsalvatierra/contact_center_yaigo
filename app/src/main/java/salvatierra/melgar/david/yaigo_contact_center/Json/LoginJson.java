package salvatierra.melgar.david.yaigo_contact_center.Json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginJson {
    @SerializedName("FuncionarioId")
    @Expose
    public Integer FuncionarioId;
    @SerializedName("Nombre")
    @Expose
    public String Nombre;
    @SerializedName("Apellido")
    @Expose
    public  String Apellido;
    @SerializedName("NombreUsuario")
    @Expose
    public  String NombreUsuario;
    @SerializedName("CedulaIdentidad")
    @Expose
    public String CedulaIdentidad;
    @SerializedName("FotoPerfil")
    @Expose
    public  String FotoPerfil;


    public Integer getfuncionarioId() {
        return FuncionarioId;
    }
    public void setfuncionarioId(Integer FuncionarioId) {
        this.FuncionarioId = FuncionarioId;
    }
    public LoginJson withfuncionarioId(Integer FuncionarioId) {
        this.FuncionarioId = FuncionarioId;
        return this;
    }

    public String getnombre() {
        return Nombre;
    }
    public void setnombre(String Nombre) {
        this.Nombre = Nombre;
    }
    public LoginJson withnombre(String Nombre) {
        this.Nombre = Nombre;
        return this;
    }

    public String getapellido() {
        return Apellido;
    }
    public void setapellido(String Apellido) {
        this.Apellido = Apellido;
    }
    public LoginJson withapellido(String Apellido) {
        this.Apellido = Apellido;
        return this;
    }

    public String getnombreUsuario() {
        return NombreUsuario;
    }
    public void setnombreUsuario(String nombreUsuario) {
        this. NombreUsuario =  NombreUsuario;
    }
    public LoginJson withnombreUsuario(String nombreUsuario) {
        this.NombreUsuario = nombreUsuario;
        return this;
    }

    public String getcedulaIdentidad() {
        return CedulaIdentidad;
    }
    public void setcedulaIdentidad(String CedulaIdentidad) {
        this.CedulaIdentidad = CedulaIdentidad;
    }
    public LoginJson withcedulaIdentidad(String CedulaIdentidad) {
        this.CedulaIdentidad = CedulaIdentidad;
        return this;
    }

    public String getfotoPerfil() {
        return FotoPerfil;
    }
    public void setfotoPerfil(String FotoPerfil) {
        this.FotoPerfil = FotoPerfil;
    }
    public LoginJson withfotoPerfil(String FotoPerfil) {
        this.FotoPerfil = FotoPerfil;
        return this;
    }
}
