package salvatierra.melgar.david.yaigo_contact_center.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import salvatierra.melgar.david.yaigo_contact_center.R;
import salvatierra.melgar.david.yaigo_contact_center.Utilitis.Preferences;

public class Inicio_activity extends AppCompatActivity {

    Preferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
        preferences = new Preferences(Inicio_activity.this);
        preferences.getiniciarSesion();
        if(preferences.getiniciarSesion()){
            //rededcionar los chat
            startActivity(new Intent(this, Activity_Menulateral.class));
        }
        else{
            startActivity(new Intent(this,Activity_Login.class));
        }
    }
}