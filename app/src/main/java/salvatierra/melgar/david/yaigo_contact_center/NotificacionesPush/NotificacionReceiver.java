package salvatierra.melgar.david.yaigo_contact_center.NotificacionesPush;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import salvatierra.melgar.david.yaigo_contact_center.Activity.Activity_Login;
import salvatierra.melgar.david.yaigo_contact_center.Activity.Activity_Menulateral;
import salvatierra.melgar.david.yaigo_contact_center.R;
import salvatierra.melgar.david.yaigo_contact_center.Utilitis.Preferences;

public class NotificacionReceiver extends BroadcastReceiver {
    public static final int REQUEST_CODE = 100;
    @Override
    public void onReceive(Context context, Intent intent) {
        Preferences preferences = new Preferences(context);
        String titulo = preferences.getnombreNotificacion() + " " + intent.getStringExtra("title");
        String cuerpo = intent.getStringExtra("alert");
        String urlimagen = intent.getStringExtra("ulrimg");
        int idContenido = Integer.parseInt(intent.getStringExtra("idContenido"));
        String tipo = intent.getStringExtra("tipo");
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.getApplicationContext().NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context,"");
        Intent i = null;
        if(preferences.getiniciarSesion()) {
            i = new Intent(context, Activity_Menulateral.class);
            //i.putExtra("idNotificacion", true);
            //i.putExtra("idContenido", idContenido);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        } else {
            i = new Intent(context, Activity_Login.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);

        // Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Uri defaultSound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.abc);
        //int resID = context.getResources().getIdentifier("ring", "raw", context.getPackageName());
        //MediaPlayer mediaPlayer = MediaPlayer.create(context, resID);
        //mediaPlayer.start();
        String channelId = "salvatierra.melgar.david.yaigo_contact_center";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .build();
            int importancia = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(channelId, titulo, importancia);
            mChannel.setDescription(titulo);
            mChannel.enableLights(true);
            mChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            mChannel.setVibrationPattern(new long[] { 1000, 1000, 1000, 1000, 1000});
            mChannel.enableVibration(true);
            mChannel.setSound(defaultSound, audioAttributes);
            notificationManager.createNotificationChannel(mChannel);
            mBuilder = new NotificationCompat.Builder(context, channelId);

        }
        mBuilder.setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(titulo)
                .setLights(Color.RED, 1, 0)//LUCES LED COLOR ROJO
                .setAutoCancel(true)
                //.setContentInfo("Noticia")
                .setSound(defaultSound)
                .setPriority(Notification.PRIORITY_HIGH)
                .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000})
                .setContentText(cuerpo)
                .setContentIntent(pendingIntent);

        if (urlimagen.length() > 0) {
            NotificationCompat.BigPictureStyle notifBig = new NotificationCompat.BigPictureStyle(mBuilder);

            byte[] byteArray = intent.getByteArrayExtra("image");
            Bitmap image = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            notifBig.bigPicture(image)
                    //.bigLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                    .bigLargeIcon(image)
                    .setBigContentTitle(titulo)
                    .setSummaryText(cuerpo)
                    .build();

        }
        notificationManager.notify(intent.getIntExtra("idNotificacion", 0), mBuilder.build());

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            PendingIntent pendingIntent1 = PendingIntent.getBroadcast(context, NotificacionReceiver.REQUEST_CODE,
                    intent, PendingIntent.FLAG_UPDATE_CURRENT);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                System.out.println("TEST - PROGRAMANDO SIGUIENTE ALARMA");
                AlarmManager.AlarmClockInfo ac= new AlarmManager.AlarmClockInfo(System.currentTimeMillis() + 30000,
                        pendingIntent1);
                alarmManager.setAlarmClock(ac, pendingIntent1);
            } else {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP,
                        System.currentTimeMillis() + (30 * 1000), pendingIntent1);
            }
        } else {
//            System.out.println("TEST - No se programó otra alarma");
        }
    }
}
