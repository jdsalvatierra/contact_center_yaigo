package salvatierra.melgar.david.yaigo_contact_center.Json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListarMensajePorHiloJson {
    @SerializedName("Mensaje")
    @Expose
    public String Mensaje;
    @SerializedName("Archivo")
    @Expose
    public String Archivo;
    @SerializedName("FechaCreacion")
    @Expose
    public String FechaCreacion;
    @SerializedName("Funcionario")
    @Expose
    public Funcionario Funcionario;
    @SerializedName("Cliente")
    @Expose
    public Cliente Cliente;
    @SerializedName("Evento")
    @Expose
    public Evento Evento;


    public class Funcionario{
        @SerializedName("FuncionarioId")
        @Expose
        public int FuncionarioId;
        @SerializedName("Nombre")
        @Expose
        public String Nombre;
        @SerializedName("Apellido")
        @Expose
        public String Apellido;
        @SerializedName("Ci")
        @Expose
        public String Ci;
        @SerializedName("FotoPefil")
        @Expose
        public String FotoPefil;

        public int getFuncionarioId() {
            return FuncionarioId;
        }

        public void setFuncionarioId(int funcionarioId) {
            FuncionarioId = funcionarioId;
        }

        public String getNombre() {
            return Nombre;
        }

        public void setNombre(String nombre) {
            Nombre = nombre;
        }

        public String getApellido() {
            return Apellido;
        }

        public void setApellido(String apellido) {
            Apellido = apellido;
        }

        public String getCi() {
            return Ci;
        }

        public void setCi(String ci) {
            Ci = ci;
        }

        public String getFotoPefil() {
            return FotoPefil;
        }

        public void setFotoPefil(String fotoPefil) {
            FotoPefil = fotoPefil;
        }
    }






    public class Evento{
        @SerializedName("EventoId")
        @Expose
        public Integer EventoId;
        @SerializedName("FechaCreacion")
        @Expose
        public String FechaCreacion;
        @SerializedName("EstadoEvento")
        @Expose
        public ListarMensajePorHiloJson.EstadoEvento estadoEvento;
        @SerializedName("TipoAtencion")
        @Expose
        public ListarMensajePorHiloJson.TipoAtencion tipoAtencion;

        public Integer getEventoId() {
            return EventoId;
        }

        public void setEventoId(Integer eventoId) {
            EventoId = eventoId;
        }

        public String getFechaCreacion() {
            return FechaCreacion;
        }

        public void setFechaCreacion(String fechaCreacion) {
            FechaCreacion = fechaCreacion;
        }

        public ListarMensajePorHiloJson.EstadoEvento getEstadoEvento() {
            return estadoEvento;
        }

        public void setEstadoEvento(ListarMensajePorHiloJson.EstadoEvento estadoEvento) {
            this.estadoEvento = estadoEvento;
        }

        public ListarMensajePorHiloJson.TipoAtencion getTipoAtencion() {
            return tipoAtencion;
        }

        public void setTipoAtencion(ListarMensajePorHiloJson.TipoAtencion tipoAtencion) {
            this.tipoAtencion = tipoAtencion;
        }
    }

    public class EstadoEvento{
        @SerializedName("EstadoEventoId")
        @Expose
        public Integer EstadoEventoId;
        @SerializedName("Nombre")
        @Expose
        public String Nombre;
        @SerializedName("Descripcion")
        @Expose
        public String Descripcion;

        public Integer getEstadoEventoId() {
            return EstadoEventoId;
        }

        public void setEstadoEventoId(Integer estadoEventoId) {
            EstadoEventoId = estadoEventoId;
        }

        public String getNombre() {
            return Nombre;
        }

        public void setNombre(String nombre) {
            Nombre = nombre;
        }

        public String getDescripcion() {
            return Descripcion;
        }

        public void setDescripcion(String descripcion) {
            Descripcion = descripcion;
        }
    }

    public class TipoAtencion{
        @SerializedName("TipoAtencionId")
        @Expose
        public Integer TipoAtencionId;
        @SerializedName("Nombre")
        @Expose
        public String Nombre;
        @SerializedName("Estado")
        @Expose
        public Integer Estado;
        @SerializedName("Area")
        @Expose
        public ListarMensajePorHiloJson.Area area;
        @SerializedName("Color")
        @Expose
        public String Color;

        public Integer getTipoAtencionId() {
            return TipoAtencionId;
        }

        public void setTipoAtencionId(Integer tipoAtencionId) {
            TipoAtencionId = tipoAtencionId;
        }

        public String getNombre() {
            return Nombre;
        }

        public void setNombre(String nombre) {
            Nombre = nombre;
        }

        public Integer getEstado() {
            return Estado;
        }

        public void setEstado(Integer estado) {
            Estado = estado;
        }

        public ListarMensajePorHiloJson.Area getArea() {
            return area;
        }

        public void setArea(ListarMensajePorHiloJson.Area area) {
            this.area = area;
        }

        public String getColor() {
            return Color;
        }

        public void setColor(String color) {
            Color = color;
        }
    }

    public class Area{
        @SerializedName("AreaId")
        @Expose
        public Integer AreaId;
        @SerializedName("Nombre")
        @Expose
        public String Nombre;
        @SerializedName("EmpresaId")
        @Expose
        public Integer EmpresaId;
        @SerializedName("EmpresaNombre")
        @Expose
        public String EmpresaNombre;

        public Integer getAreaId() {
            return AreaId;
        }

        public void setAreaId(Integer areaId) {
            AreaId = areaId;
        }

        public String getNombre() {
            return Nombre;
        }

        public void setNombre(String nombre) {
            Nombre = nombre;
        }

        public Integer getEmpresaId() {
            return EmpresaId;
        }

        public void setEmpresaId(Integer empresaId) {
            EmpresaId = empresaId;
        }

        public String getEmpresaNombre() {
            return EmpresaNombre;
        }

        public void setEmpresaNombre(String empresaNombre) {
            EmpresaNombre = empresaNombre;
        }
    }




    public class Cliente{
        @SerializedName("ClienteId")
        @Expose
        public int ClienteId;
        @SerializedName("Nombres")
        @Expose
        public String Nombres;
        @SerializedName("Apellidos")
        @Expose
        public String Apellidos;
        @SerializedName("Correo")
        @Expose
        public String Correo;
        @SerializedName("UrlFoto")
        @Expose
        public String UrlFoto;

        public int getClienteId() {
            return ClienteId;
        }

        public void setClienteId(int clienteId) {
            ClienteId = clienteId;
        }

        public String getNombres() {
            return Nombres;
        }

        public void setNombres(String nombres) {
            Nombres = nombres;
        }

        public String getApellidos() {
            return Apellidos;
        }

        public void setApellidos(String apellidos) {
            Apellidos = apellidos;
        }

        public String getCorreo() {
            return Correo;
        }

        public void setCorreo(String correo) {
            Correo = correo;
        }

        public String getUrlFoto() {
            return UrlFoto;
        }

        public void setUrlFoto(String urlFoto) {
            UrlFoto = urlFoto;
        }
    }

    public String getMensaje() {
        return Mensaje;
    }

    public void setMensaje(String mensaje) {
        Mensaje = mensaje;
    }

    public String getArchivo() {
        return Archivo;
    }

    public void setArchivo(String archivo) {
        Archivo = archivo;
    }

    public String getFechaCreacion() {
        return FechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        FechaCreacion = fechaCreacion;
    }

    public ListarMensajePorHiloJson.Funcionario getFuncionario() {
        return Funcionario;
    }

    public void setFuncionario(ListarMensajePorHiloJson.Funcionario funcionario) {
        this.Funcionario = funcionario;
    }

    public ListarMensajePorHiloJson.Cliente getCliente() {
        return Cliente;
    }

    public void setCliente(ListarMensajePorHiloJson.Cliente cliente) {
        this.Cliente = cliente;
    }

    public ListarMensajePorHiloJson.Evento getEvento() {
        return Evento;
    }

    public void setEvento(ListarMensajePorHiloJson.Evento evento) {
        this.Evento = evento;
    }
}
