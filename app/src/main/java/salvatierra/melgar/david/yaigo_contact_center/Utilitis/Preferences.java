package salvatierra.melgar.david.yaigo_contact_center.Utilitis;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {
      SharedPreferences preferences;

    public Preferences(Context context) {
        preferences = context.getSharedPreferences("DatosUsuario",Context.MODE_PRIVATE);
    }


    public boolean getiniciarSesion() {
        return preferences.getBoolean("iniciarSesion", false);
    }
    public String getnombreNotificacion() {
        return preferences.getString("Nombre", "");
    }

    public static Integer getfuncionarioId(SharedPreferences preferences) {
        return preferences.getInt("FuncionarioId",0);
    }
    public static String getnombre(SharedPreferences preferences) {
        return preferences.getString("Nombre", "");
    }

    public static String getapellido(SharedPreferences preferences) {
        return preferences.getString("Apellido", "");
    }
    public static String getnombreUsuario(SharedPreferences preferences) {
        return preferences.getString("NombreUsuario", "");
    }
    public static String getfotoPerfil(SharedPreferences preferences) {
        return preferences.getString("FotoPerfil", "");
    }
    public static String getcedulaIdentidad(SharedPreferences preferences){
        return preferences.getString("CedulaIdentidad","");
    }

    public void borrarPrefrencias(){
        preferences.edit().clear().apply();
    }
}

