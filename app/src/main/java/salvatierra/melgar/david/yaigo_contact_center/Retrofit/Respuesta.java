package salvatierra.melgar.david.yaigo_contact_center.Retrofit;

public class Respuesta <T> {
    public int Codigo;
    public String FechaHora;
    public String Mensaje;
    public T Data;

    public int getCodigo() {
        return Codigo;
    }

    public void setCodigo(int codigo) {
        Codigo = codigo;
    }

    public String getFechaHora() {
        return FechaHora;
    }

    public void setFechaHora(String fechaHora) {
        FechaHora = fechaHora;
    }

    public String getMensaje() {
        return Mensaje;
    }

    public void setMensaje(String mensaje) {
        Mensaje = mensaje;
    }

    public T getData() {
        return Data;
    }

    public void setData(T data) {
        Data = data;
    }

    public boolean respuestaExitosa() { return Codigo==0;
    }
}
